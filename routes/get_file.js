const private_config = require('../../distribution_config/private.json')
const file_store_driver = require('../../file_store_bk_driver/index')
const JWT = require('jsonwebtoken')
const mongodbObjectId = require('mongodb').ObjectId

module.exports = [
    {
        method: 'POST',
        url: '/get_file/owned_file\*',
        schema:{
            body:{
                type: 'object',
                properties: {
                    user_token:{type:'string', maxLength:1000, minLength:3},
                    file_token:{type:'string', maxLength:1000, minLength:3}
                }
            }
        },
        handler: async (request, reply) => {
            const body = request.body
            try{
                const user = JWT.verify(body.user_token, private_config.user_token_key)
                const all_about_file = JWT.verify(body.file_token, private_config.file_access_token_key)
                console.log(user);
                if( (user.userId != all_about_file.owner_id) && (!all_about_file.allow_users.includes(user.userId)) ){
                    throw Error("Permissions denied")
                }
                //using public method for optimized performance
                //security uncompromised as lang as current layers can verify the user & file token
                const file_buffer = await file_store_driver.GetFile.select_file_as_public(all_about_file.file_id)
                reply.header('Content-Type', all_about_file.mimetype).send(file_buffer.buffer)
            }catch(e){
                reply.code(301)
                return {error:e.message, result:null}
            }
        }
    },
    {
        method: 'POST',
        url: '/get_file_details/owned_file\*',
        schema:{
            body:{
                type: 'object',
                properties: {
                    user_token:{type:'string', maxLength:1000, minLength:3},
                    file_token:{type:'string', maxLength:1000, minLength:3}
                }
            }
        },
        handler: async (request, reply) => {
            const body = request.body
            try{
                const user = JWT.verify(body.user_token, private_config.user_token_key)
                const all_about_file = JWT.verify(body.file_token, private_config.file_access_token_key)
                console.log(user);
                if( (user.userId != all_about_file.owner_id) && (!all_about_file.allow_users.includes(user.userId)) ){
                    throw Error("Permissions denied")
                }
                //using public method for optimized performance
                //security uncompromised as lang as current layers can verify the user & file token
                //const file_buffer = await file_store_driver.GetFile.select_file_as_public(all_about_file.file_id)
                reply.send(all_about_file)
            }catch(e){
                reply.code(301)
                return {error:e.message, result:null}
            }
        }
    }
]