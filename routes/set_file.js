const private_config = require('../../distribution_config/private.json')
const file_store_driver = require('../../file_store_bk_driver/index')
const JWT = require('jsonwebtoken')
const mongodbObjectId = require('mongodb').ObjectId
 
module.exports = [
    {
        method: 'POST',
        url: '/set_file/owned_file',
        handler: async (request, reply) => {
            try{
                const file = await request.file()
                const fields = file.fields
                if(fields.user_token.value.length>1000 || fields.user_token.value<5){
                    throw Error("Invalid token")
                }
                //console.log(fields)
                const user = JWT.verify(fields.user_token.value, private_config.user_token_key)
                const file_buffer = await file.fields.file.toBuffer()
                const allow_users_set = new Set()
                if(fields.allow_users && fields.allow_users.value.length>23){
                    fields.allow_users.value.split(',').forEach(element => {
                        try{
                            allow_users_set.add(mongodbObjectId(element.replace(/\s/g, '')).toString())
                        }catch{}
                    });
                }
                const about_file = {
                    filename:file.fields.file.filename,
                    encoding:file.fields.file.encoding,
                    mimetype:file.fields.file.mimetype,
                    size:file_buffer.length,
                    allow_users:[...allow_users_set]
                }
                const file_id = await file_store_driver.SetFile.insert_new_file(file_buffer, user.userId)
                var all_about_file = {
                    about_file  : about_file,
                    file_token  : JWT.sign(Object.assign(about_file, file_id), private_config.file_access_token_key)
                }
                //console.log("Complete", all_about_file);
                return all_about_file
            }catch(e){
                reply.code(504)
                console.log(e);
                return {error:e.message, result:null}
            }
        }
    },
    {
        method: 'POST',
        url: '/expire_file/owned_file',
        schema:{
            body:{
                type: 'object',
                properties: {
                    user_token:{type:'string', maxLength:1000, minLength:3},
                    file_token:{type:'string', maxLength:1000, minLength:3}
                }
            }
        },
        handler: async (request, reply) => {
            const body = request.body
            try{
                const user = JWT.verify(body.user_token, private_config.user_token_key)
                const all_about_file = JWT.verify(body.file_token, private_config.file_access_token_key)
                console.log(user);
                if( (user.userId != all_about_file.owner_id) ){
                    throw Error("Permissions denied")
                }

                const res = await file_store_driver.SetFile.expire_file_as_owner_safe(user.userId, all_about_file.file_id)
                reply.send({res})
            }catch(e){
                reply.code(301)
                return {error:e.message, result:null}
            }
        }
    }
]